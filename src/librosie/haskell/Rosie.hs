--  Rosie.hs     An interface to librosie from Haskell
--
--  © Copyright IBM Corporation 2018.
--  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
--  AUTHOR: Laurence Emms

{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE OverloadedStrings #-}

module Rosie (RosieStr(..),
              RosieMatch(..),
              RosieParsedMatch(..),
              RosieEngine(..),
              RosiePackage(..),
              RosieException(..),
              -- Helper functions
              rosieStringLength,
              rosieStringChars,
              unsafeNewEmptyRosieString,
              rosieMatchLength,
              rosieMatchChars,
              parseMatch,
              -- Unsafe C FFI functions
              cRosieEmptyString,
              cRosieNewString,
              cRosieFreeStringPtr,
              cRosieNew,
              cRosieFinalize,
              cRosieLibpath,
              cRosieAllocLimit,
              cRosieConfig,
              cRosieCompile,
              cRosieFreeRplx,
              cRosieMatch,
              cRosieMatchfile,
              cRosieTrace,
              cRosieLoad,
              cRosieLoadfile,
              cRosieImport,
              cRosieReadRcFile,
              cRosieExecuteRcfile,
              cRosieExpressionRefs,
              cRosieBlockRefs,
              cRosieExpressionDeps,
              cRosieBlockDeps,
              cRosieParseExpression,
              cRosieParseBlock,
              -- Safer functions
              emptyRosieString,
              newRosieString,
              emptyRosieMatch,
              newRosieEngine,
              loadEngine,
              compileExpression,
              allocLimit,
              config,
              importPackageAs,
              importPackage,
              loadEngineFromFile,
              matchPattern,
              -- Tests
              testRosie) where

import Data.Word (Word8, Word32)
import Data.Int (Int32)
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Internal as BI
import Data.Typeable
import Foreign.C
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Storable
import Foreign.Marshal.Utils
import Foreign.Marshal.Alloc

import Control.Exception.Safe (Exception, MonadThrow, SomeException, throwM, try, tryIO, catch, catchIO, finally, onException)
import Control.Monad (join)
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Control.Monad.IO.Class (MonadIO, liftIO)

import Data.Aeson

-- Rosie C types
--
-- size_t
-- byte_ptr == unsigned char*
-- str == struct rosie_string
-- Engine == struct rosie_engine
-- match == struct rosie_matchresult

data RosieStr = RosieStr
                { len :: Word32
                , ptr :: CString
                } deriving (Show, Eq)

instance Storable RosieStr where
    alignment _ = 8
    sizeOf _    = 16
    peek ptr    = RosieStr
        <$> peekByteOff ptr 0
        <*> peekByteOff ptr 8
    poke ptr (RosieStr l p) = do
        pokeByteOff ptr 0 l
        pokeByteOff ptr 8 p

rosieStringLength :: RosieStr -> Word32
rosieStringLength (RosieStr { len = thisLen }) = thisLen

rosieStringChars :: RosieStr -> CStringLen
rosieStringChars (RosieStr { len = thisLen, ptr = thisPtr }) = (thisPtr, fromInteger (toInteger thisLen))

unsafeNewEmptyRosieString :: IO (Ptr RosieStr)
unsafeNewEmptyRosieString = new (RosieStr { len = 0, ptr = nullPtr })

data RosieMatch = RosieMatch
                  { match :: RosieStr
                  , leftover :: Int32
                  , abend :: Int32
                  , ttotal :: Int32
                  , tmatch :: Int32
                  } deriving (Show, Eq)

instance Storable RosieMatch where
    alignment _ = 8
    sizeOf _    = 32
    peek ptr    = RosieMatch
        <$> peekByteOff ptr 0
        <*> peekByteOff ptr 16
        <*> peekByteOff ptr 20
        <*> peekByteOff ptr 24
        <*> peekByteOff ptr 28
    poke ptr (RosieMatch m l a t tm) = do
        pokeByteOff ptr 0 m
        pokeByteOff ptr 16 l
        pokeByteOff ptr 20 a
        pokeByteOff ptr 24 t
        pokeByteOff ptr 28 tm

rosieMatchLength :: RosieMatch -> Word32
rosieMatchLength (RosieMatch {match = (RosieStr { len = thisLen })}) = thisLen

rosieMatchChars :: RosieMatch -> CStringLen
rosieMatchChars (RosieMatch {match = (RosieStr { len = thisLen, ptr = thisPtr })}) = (thisPtr, fromInteger (toInteger thisLen))

-- Code for parsing matches

data RosieParsedMatch = RosieParsedMatch { matchType :: !T.Text
                                         , start :: Int
                                         , end :: Int
                                         , matchData :: !T.Text
                                         } deriving (Show)

instance FromJSON RosieParsedMatch where
    parseJSON = withObject "RosieParsedMatch" $ \v -> RosieParsedMatch
        <$> v .: "type"
        <*> v .: "s"
        <*> v .: "e"
        <*> v .: "data"

parseMatch :: String -> Maybe RosieParsedMatch
parseMatch s = decode (B.pack $ fmap BI.c2w s) :: Maybe RosieParsedMatch

-- RosieEngine phantom for Ptr
data RosieEngine = RosieEngine

-- Unsafe C functions
-- The arguments of these functions can mutate

-- Any argument with (Malloc) is malloced by the function

-- Any argument with (Free) is freed by the function

-- Any argument with (Empty) must be allocated but empty when passed to the function,
-- arguments which are not empty will result in a memory leak

-- Any argument with (Allocated) must be allocated before being passed to the function

-- Arguments with (Empty/Allocated) can be either empty or allocated when passed to the
-- function

-- Helper function to allocate an empty string
cRosieEmptyString :: (MonadIO m) => m (Ptr RosieStr) -- (Malloc)
cRosieEmptyString = liftIO $ calloc

foreign import ccall "rosie_new_string_ptr" cRosieNewString :: CString -- msg
                                                            -> Word32 -- len
                                                            -> IO (Ptr RosieStr) -- (Malloc)
foreign import ccall "rosie_free_string_ptr" cRosieFreeStringPtr :: Ptr RosieStr -- ref (Free)
                                                                 -> IO ()

foreign import ccall "rosie_new" cRosieNew :: Ptr RosieStr -- messages (Empty)
                                           -> IO (Ptr RosieEngine) -- (Malloc)
foreign import ccall "rosie_finalize" cRosieFinalize :: Ptr RosieEngine -- e (Free)
                                                     -> IO ()
foreign import ccall "rosie_libpath" cRosieLibpath :: Ptr RosieEngine -- e (Allocated)
                                                   -> Ptr RosieStr -- newpath (Empty/Allocated)
                                                   -> IO Int32
foreign import ccall "rosie_alloc_limit" cRosieAllocLimit :: Ptr RosieEngine -- e (Allocated)
                                                          -> Ptr Int32 -- newlimit (Empty/Allocated)
                                                          -> Ptr Int32 -- usage (Empty/Allocated)
                                                          -> IO Int32
foreign import ccall "rosie_config" cRosieConfig :: Ptr RosieEngine -- e (Allocated)
                                                 -> Ptr RosieStr -- retval (Empty)
                                                 -> IO Int32
foreign import ccall "rosie_compile" cRosieCompile :: Ptr RosieEngine -- e (Allocated)
                                                   -> Ptr RosieStr -- expression (Allocated)
                                                   -> Ptr Int32 -- pat (Allocated)
                                                   -> Ptr RosieStr -- messages (Empty)
                                                   -> IO Int32
foreign import ccall "rosie_free_rplx" cRosieFreeRplx :: Ptr RosieEngine -- e (Allocated)
                                                      -> Int32 -- pat
                                                      -> IO Int32
-- Helper function to allocate an empty match
cRosieEmptyMatch :: (MonadIO m) => m (Ptr RosieMatch) -- (Malloc)
cRosieEmptyMatch = liftIO $ calloc

foreign import ccall "rosie_match" cRosieMatch :: Ptr RosieEngine -- e (Allocated)
                                               -> Int32 -- pat
                                               -> Int32 -- start
                                               -> CString -- encoder_name (Allocated)
                                               -> Ptr RosieStr -- input (Allocated)
                                               -> Ptr RosieMatch -- match (Empty)
                                               -> IO Int32
foreign import ccall "rosie_matchfile" cRosieMatchfile :: Ptr RosieEngine -- e (Allocated)
                                                       -> Int32 -- pat
                                                       -> CString -- encoder (Allocated)
                                                       -> Int32 -- wholefileflag
                                                       -> CString -- infilename (Allocated)
                                                       -> CString -- outfilename (Allocated)
                                                       -> CString -- errfilename (Allocated)
                                                       -> Ptr Int32 -- cin (Allocated)
                                                       -> Ptr Int32 -- cout (Allocated)
                                                       -> Ptr Int32 -- cerr (Allocated)
                                                       -> Ptr RosieStr -- err (Empty)
                                                       -> IO Int32
foreign import ccall "rosie_trace" cRosieTrace :: Ptr RosieEngine -- e (Allocated)
                                               -> Int32 -- pat
                                               -> Int32 -- start
                                               -> CString -- trace_style (Allocated)
                                               -> Ptr RosieStr -- input (Allocated)
                                               -> Ptr Int32 -- matched (Allocated)
                                               -> Ptr RosieStr -- trace (Empty)
                                               -> IO Int32
foreign import ccall "rosie_load" cRosieLoad :: Ptr RosieEngine -- e (Allocated)
                                             -> Ptr Int32 -- ok (Allocated)
                                             -> Ptr RosieStr -- src (Allocated)
                                             -> Ptr RosieStr -- pkgname (Empty)
                                             -> Ptr RosieStr -- messages (Empty)
                                             -> IO Int32
foreign import ccall "rosie_loadfile" cRosieLoadfile :: Ptr RosieEngine -- e (Allocated)
                                                     -> Ptr Int32 -- ok (Allocated)
                                                     -> Ptr RosieStr -- fn (Allocated)
                                                     -> Ptr RosieStr -- pkgname (Empty)
                                                     -> Ptr RosieStr -- messages (Empty)
                                                     -> IO Int32
foreign import ccall "rosie_import" cRosieImport :: Ptr RosieEngine -- e (Allocated)
                                                 -> Ptr Int32 -- ok (Allocated)
                                                 -> Ptr RosieStr -- pkgname (Allocated)
                                                 -> Ptr RosieStr -- as (Empty/Allocated)
                                                 -> Ptr RosieStr -- actual_pkgname (Empty)
                                                 -> Ptr RosieStr -- messages (Empty)
                                                 -> IO Int32
foreign import ccall "rosie_read_rcfile" cRosieReadRcFile :: Ptr RosieEngine -- e (Allocated)
                                                          -> Ptr RosieStr -- filename (Allocated)
                                                          -> Ptr Int32 -- file_exists (Allocated)
                                                          -> Ptr RosieStr -- options (Empty)
                                                          -> Ptr RosieStr -- messages (Empty)
                                                          -> IO Int32
foreign import ccall "rosie_execute_rcfile" cRosieExecuteRcfile :: Ptr RosieEngine -- e (Allocated)
                                                                -> Ptr RosieStr -- filename (Allocated)
                                                                -> Ptr Int32 -- file_exists (Allocated)
                                                                -> Ptr Int32 -- no_errors (Allocated)
                                                                -> Ptr RosieStr -- messages (Empty)
                                                                -> IO Int32

foreign import ccall "rosie_expression_refs" cRosieExpressionRefs :: Ptr RosieEngine -- e (Allocated)
                                                                  -> Ptr RosieStr -- input (Allocated)
                                                                  -> Ptr RosieStr -- refs (Empty)
                                                                  -> Ptr RosieStr -- messages (empty)
                                                                  -> IO Int32
foreign import ccall "rosie_block_refs" cRosieBlockRefs :: Ptr RosieEngine -- e (Allocated)
                                                        -> Ptr RosieStr -- input (Allocated)
                                                        -> Ptr RosieStr -- refs (Empty)
                                                        -> Ptr RosieStr -- messages (Empty)
                                                        -> IO Int32
foreign import ccall "rosie_expression_deps" cRosieExpressionDeps :: Ptr RosieEngine -- e (Allocated)
                                                                  -> Ptr RosieStr -- input (Allocated)
                                                                  -> Ptr RosieStr -- refs (Empty)
                                                                  -> Ptr RosieStr -- messages (Empty)
                                                                  -> IO Int32
foreign import ccall "rosie_block_deps" cRosieBlockDeps :: Ptr RosieEngine -- e (Allocated)
                                                        -> Ptr RosieStr -- input (Allocated)
                                                        -> Ptr RosieStr -- refs (Empty)
                                                        -> Ptr RosieStr -- messages (Empty)
                                                        -> IO Int32
foreign import ccall "rosie_parse_expression" cRosieParseExpression :: Ptr RosieEngine -- e (Allocated)
                                                                    -> Ptr RosieStr -- input (Allocated)
                                                                    -> Ptr RosieStr -- refs (Empty)
                                                                    -> Ptr RosieStr -- messages (Empty)
                                                                    -> IO Int32
foreign import ccall "rosie_parse_block" cRosieParseBlock :: Ptr RosieEngine -- e (Allocated)
                                                          -> Ptr RosieStr -- input (Allocated)
                                                          -> Ptr RosieStr -- refs (Empty)
                                                          -> Ptr RosieStr -- messages (Empty)
                                                          -> IO Int32

-- Safer interface
-- This adds managed memory to the C functions

-- This adds exception support
data RosieException = SourceException String
                    | LoadException String
                    | PackageNameException String
                    | CompileException String
                    | AlloclimitException String
                    | EngineException String
                    | ConfigException String
                    | ImportException String
                    | TestException String String
                    deriving (Show, Typeable)

instance Exception RosieException

foreign import ccall "&rosie_free_string_ptr" ptrRosieFreeStringPtr :: FunPtr (Ptr RosieStr ->  IO ())
foreign import ccall "&rosie_finalize" ptrRosieFinalize :: FunPtr (Ptr RosieEngine -> IO ())

type FPtrRosieStr = ForeignPtr RosieStr 
type FPtrRosieMatch = ForeignPtr RosieMatch 
type FPtrRosieEngine = ForeignPtr RosieEngine 

emptyRosieString :: (MonadIO m) => m FPtrRosieStr
emptyRosieString = do
    stringPtr <- cRosieEmptyString
    liftIO $ newForeignPtr ptrRosieFreeStringPtr stringPtr

newRosieString :: (MonadIO m) => String -> m FPtrRosieStr
newRosieString s = do
    let l = fromIntegral (length s)
    stringPtr <- liftIO $ withCString s (\cString -> cRosieNewString cString l)
    liftIO $ newForeignPtr ptrRosieFreeStringPtr stringPtr

emptyRosieMatch :: (MonadIO m) => m FPtrRosieMatch
emptyRosieMatch = do
    matchPtr <- cRosieEmptyMatch
    liftIO $ newForeignPtr finalizerFree matchPtr

newRosieEngine :: (MonadIO m) => m FPtrRosieEngine
newRosieEngine = liftIO $ do
    messagesPtr <- cRosieEmptyString
    enginePtr <- cRosieNew messagesPtr
    if enginePtr == nullPtr
    then do
        hM <- peek messagesPtr
        if (rosieStringLength hM) == 0
        then throwM $ EngineException "Failed to make engine"
        else do
            hMessages <- peekCStringLen (rosieStringChars hM)
            throwM $ EngineException hMessages
    else liftIO $ newForeignPtr ptrRosieFinalize enginePtr

data RosiePackage = RosiePackage { engine :: FPtrRosieEngine
                                 , packageName :: FPtrRosieStr
                                 } deriving (Show, Typeable)

loadEngine :: (MonadIO m) => FPtrRosieStr -> m (FPtrRosieEngine, FPtrRosieStr)
loadEngine sourceFPtr = liftIO $ do
    engineFPtr <- newRosieEngine
    packageNameFPtr <- emptyRosieString
    errorsFPtr <- emptyRosieString
    liftIO $  withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr sourceFPtr (\sourcePtr -> do
                withForeignPtr packageNameFPtr (\packageNamePtr ->
                    withForeignPtr errorsFPtr (\errorsPtr -> do
                        hS <- peek sourcePtr
                        if (rosieStringLength hS) == 0
                        then do
                            throwM $ SourceException "Invalid Source"
                        else do
                            errorCode <- cRosieLoad enginePtr okPtr sourcePtr packageNamePtr errorsPtr
                            hOk <- peek okPtr
                            if errorCode /= 0 || hOk == 0
                            then do
                                hErr <- peek errorsPtr
                                if (rosieStringLength hErr) /= 0
                                then do
                                    hErrors <- peekCStringLen (rosieStringChars hErr)
                                    throwM $ LoadException (hErrors ++ " Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                                else throwM $ LoadException ("No error message. Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                            else do
                                hPn <- peek packageNamePtr
                                if (rosieStringLength hPn) /= 0
                                then liftIO $ return (engineFPtr, packageNameFPtr)
                                else throwM $ PackageNameException "No package name")))))

compileExpression :: (MonadIO m) => FPtrRosieEngine -> FPtrRosieStr -> m (FPtrRosieEngine, Int32)
compileExpression engineFPtr expressionFPtr = liftIO $ do
    errorsFPtr <- emptyRosieString
    liftIO $  withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\patPtr ->
            withForeignPtr expressionFPtr (\expressionPtr -> do
                withForeignPtr errorsFPtr (\errorsPtr -> do
                    errorCode <- cRosieCompile enginePtr expressionPtr patPtr errorsPtr
                    if errorCode /= 0
                    then do
                        hErr <- peek errorsPtr
                        if (rosieStringLength hErr) /= 0
                        then do
                            hErrors <- peekCStringLen (rosieStringChars hErr)
                            throwM $ CompileException (hErrors ++ " Error code: " ++ (show errorCode))
                        else throwM $ CompileException ("No error message. Error code: " ++ (show errorCode))
                    else do 
                        hPat <- peek patPtr
                        return (engineFPtr, hPat)))))

allocLimit :: (MonadIO m, Integral n) => FPtrRosieEngine -> n -> m (FPtrRosieEngine, Int32, Int32)
allocLimit engineFPtr limit = do
    liftIO $ withForeignPtr engineFPtr (\enginePtr ->
        with (fromIntegral limit) (\limitPtr ->
            with (fromInteger 0) (\usagePtr -> do
                errorCode <- cRosieAllocLimit enginePtr limitPtr usagePtr
                if errorCode /= 0
                then throwM $ AlloclimitException "Failed to get alloclimit"
                else do
                    hLimit <- peek limitPtr
                    hUsage <- peek usagePtr
                    liftIO $ return (engineFPtr, hLimit, hUsage))))

config :: (MonadIO m) => FPtrRosieEngine -> m String
config engineFPtr = liftIO $ do
    configFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        withForeignPtr configFPtr (\configPtr -> do
            errorCode <- cRosieConfig enginePtr configPtr
            if errorCode /= 0
            then throwM $ ConfigException "Failed to load config"
            else do
                hC <- peek configPtr
                if (rosieStringLength hC) /= 0
                then do
                    hConfig <- peekCStringLen (rosieStringChars hC)
                    liftIO $ return hConfig
                else throwM $ ConfigException "Failed to load config"))

importPackageAs :: (MonadIO m) => FPtrRosieEngine -> FPtrRosieStr -> FPtrRosieStr -> m (FPtrRosieEngine, FPtrRosieStr)
importPackageAs engineFPtr packageNameFPtr asPtr = liftIO $ do
    asFPtr <- emptyRosieString
    actualPackageNameFPtr <- emptyRosieString
    messagesFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr packageNameFPtr (\packageNamePtr ->
                withForeignPtr asFPtr (\asPtr ->
                    withForeignPtr actualPackageNameFPtr (\actualPackageNamePtr ->
                        withForeignPtr messagesFPtr (\messagesPtr -> do
                            errorCode <- cRosieImport enginePtr okPtr packageNamePtr asPtr actualPackageNamePtr messagesPtr
                            hOk <- peek okPtr
                            if errorCode /= 0 || hOk == 0
                            then do
                                hErr <- peek messagesPtr
                                if (rosieStringLength hErr) /= 0
                                then do
                                    hErrors <- peekCStringLen (rosieStringChars hErr)
                                    throwM $ ImportException (hErrors ++ " Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                                else throwM $ ImportException ("No error message. Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                            else do
                                hPn <- peek actualPackageNamePtr
                                if (rosieStringLength hPn) /= 0
                                then do
                                    return (engineFPtr, actualPackageNameFPtr)
                                else throwM $ ImportException "Import failed: No package name"))))))

importPackage :: (MonadIO m) => FPtrRosieEngine -> FPtrRosieStr -> m (FPtrRosieEngine, FPtrRosieStr)
importPackage engineFPtr packageNameFPtr = liftIO $ do
    asFPtr <- emptyRosieString
    importPackageAs engineFPtr packageNameFPtr asFPtr

loadEngineFromFile :: (MonadIO m) => FPtrRosieStr -> m (FPtrRosieEngine, FPtrRosieStr)
loadEngineFromFile fileNameFPtr = liftIO $ do
    engineFPtr <- newRosieEngine
    packageNameFPtr <- emptyRosieString
    messagesFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr fileNameFPtr (\fileNamePtr ->
                withForeignPtr packageNameFPtr (\packageNamePtr ->
                    withForeignPtr messagesFPtr (\messagesPtr -> do
                        errorCode <- cRosieLoadfile enginePtr okPtr fileNamePtr packageNamePtr messagesPtr
                        hOk <- peek okPtr
                        if errorCode /= 0 || hOk == 0
                        then do
                            hErr <- peek messagesPtr
                            if (rosieStringLength hErr) /= 0
                            then do
                                hErrors <- peekCStringLen (rosieStringChars hErr)
                                throwM $ LoadException (hErrors ++ " Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                            else throwM $ LoadException ("No error message. Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                        else do
                            hPn <- peek packageNamePtr
                            if (rosieStringLength hPn) /= 0
                            then do
                                return (engineFPtr, packageNameFPtr)
                            else throwM $ LoadException "No package name")))))

matchPattern :: (MonadIO m) => FPtrRosieEngine -> Int32 -> Int32 -> FPtrRosieStr -> MaybeT m FPtrRosieMatch
matchPattern engineFPtr start pat inputFPtr = do
    matchFPtr <- emptyRosieMatch
    MaybeT $ liftIO $ withForeignPtr engineFPtr (\enginePtr ->
        withCString "json" (\encoder ->
            withForeignPtr inputFPtr (\inputPtr ->
                withForeignPtr matchFPtr (\matchPtr -> do
                    errorCode <- cRosieMatch enginePtr pat start encoder inputPtr matchPtr
                    if errorCode /= 0
                    then return Nothing
                    else return (Just matchFPtr)))))

-- Test run

testUnsafeInit :: IO ()
testUnsafeInit = do
    putStrLn "Testing Unsafe Initialization"
    messages <- cRosieEmptyString
    engine <- cRosieNew messages
    result <- peek messages
    let resultLength = rosieStringLength result
    if resultLength /= 0
    then do
        errorMessage <- peekCStringLen (rosieStringChars result)
        throwM $ TestException "testUnsafeInit" errorMessage
    else 
        return ()
    putStrLn "Cleaning Up"
    cRosieFinalize engine
    putStrLn "Done"

testSaferInit :: IO ()
testSaferInit = do
    putStrLn "Testing Safer Initialization"
    engine <- newRosieEngine
    putStrLn "Done"

testSaferLoad :: IO ()
testSaferLoad = do
    putStrLn "Testing Safer Loading"
    sourceFPtr <- newRosieString "package x; foo = \"foo\""
    engineFPtr <- newRosieEngine
    packageNameFPtr <- emptyRosieString
    errorsFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr sourceFPtr (\sourcePtr -> do
                withForeignPtr packageNameFPtr (\packageNamePtr ->
                    withForeignPtr errorsFPtr (\errorsPtr -> do
                        hS <- peek sourcePtr
                        if (rosieStringLength hS) == 0
                        then do
                            throwM $ TestException "testSaferLoad" "Invalid Source"
                        else do
                            hSource <- peekCStringLen (rosieStringChars hS)
                            putStrLn ("Package: " ++ hSource)
                        errorCode <- cRosieLoad enginePtr okPtr sourcePtr packageNamePtr errorsPtr
                        hOk <- peek okPtr
                        if errorCode /= 0 || hOk == 0
                        then do
                            putStrLn "Failed to load package"
                            hErr <- peek errorsPtr
                            if (rosieStringLength hErr) /= 0
                            then do
                                hErrors <- peekCStringLen (rosieStringChars hErr)
                                throwM $ TestException "testSaferLoad" hErrors
                            else throwM $ TestException "testSaferLoad" "No error message"
                        else do
                            putStrLn "Loaded package"
                            hPn <- peek packageNamePtr
                            if (rosieStringLength hPn) /= 0
                            then do
                                hPackageName <- peekCStringLen (rosieStringChars hPn)
                                if hPackageName /= "x"
                                then throwM $ TestException "testSaferLoad" ("Invalid package name. Package name: " ++ hPackageName)
                                else do
                                    putStrLn ("Package name: " ++ hPackageName)
                                    putStrLn "Done"
                            else throwM $ TestException "testSaferLoad" "No package name")))))

testMonadicLoad :: IO ()
testMonadicLoad = do
    putStrLn "Testing Monadic Loading"
    sourceFPtr <- newRosieString "package x; foo = \"foo\""
    (engineFPtr, packageNameFPtr) <- loadEngine sourceFPtr
    expression1FPtr <- newRosieString "x.foo"
    (engine2FPtr, pat1) <- compileExpression engineFPtr expression1FPtr
    putStrLn $ "Compiled pattern " ++ (show pat1)
    expression2FPtr <- newRosieString "[:digit:]+"
    (engine3FPtr, pat2) <- compileExpression engine2FPtr expression2FPtr
    putStrLn $ "Compiled pattern " ++ (show pat2)
    putStrLn "Done"

testConfig :: IO ()
testConfig = do
    putStrLn "Testing Config"
    engineFPtr <- newRosieEngine
    configFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        withForeignPtr configFPtr (\configPtr -> do
            errorCode <- cRosieConfig enginePtr configPtr
            if errorCode /= 0
            then throwM $ TestException "testConfig" "Failed to load config"
            else do
                hC <- peek configPtr
                if (rosieStringLength hC) /= 0
                then do
                    hConfig <- peekCStringLen (rosieStringChars hC)
                    putStrLn ("Config: " ++ hConfig)
                    putStrLn "Done"
                else throwM $ TestException "testConfig" "No config"))

testMonadicConfig :: IO ()
testMonadicConfig = do
    putStrLn "Testing Monadic Config"
    engineFPtr <- newRosieEngine
    cfg <- config engineFPtr
    putStrLn cfg
    putStrLn "Done"

testLibpath :: IO ()
testLibpath = do
    putStrLn "Testing Libpath"
    engineFPtr <- newRosieEngine
    libPathFPtr <- newRosieString "foo bar baz"
    withForeignPtr engineFPtr (\enginePtr ->
        withForeignPtr libPathFPtr (\libPathPtr -> do
            errorCode <- cRosieLibpath enginePtr libPathPtr
            if errorCode /= 0
            then throwM $ TestException "testLibpath" "Failed to load libpath"
            else do
                hLp <- peek libPathPtr
                if (rosieStringLength hLp) /= 0
                then do
                    hLibpath <- peekCStringLen (rosieStringChars hLp)
                    putStrLn ("Lib path: " ++ hLibpath)
                    putStrLn "Done"
                else throwM $ TestException "testLibpath" "No lib path"))

testAlloclimitUsage :: IO ()
testAlloclimitUsage = do
    putStrLn "Testing Alloclimit Usage"
    engineFPtr <- newRosieEngine
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\limitPtr ->
            with (fromInteger 0) (\usagePtr -> do
                errorCode <- cRosieAllocLimit enginePtr limitPtr usagePtr
                if errorCode /= 0
                then throwM $ TestException "testAlloclimitUsage" "Failed to get alloclimit"
                else do
                    hLimit <- peek limitPtr
                    hUsage <- peek usagePtr
                    putStrLn ("Limit: " ++ (show hLimit) ++ " Usage: " ++ (show hUsage))
                    putStrLn "Done")))

testAlloclimit :: IO ()
testAlloclimit = do
    putStrLn "Testing Alloclimit"
    engineFPtr <- newRosieEngine
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 8199) (\limitPtr ->
            with (fromInteger 0) (\usagePtr -> do
                errorCode <- cRosieAllocLimit enginePtr limitPtr usagePtr
                if errorCode /= 0
                then throwM $ TestException "testAlloclimit" "Failed to get alloclimit"
                else do
                    hLimit <- peek limitPtr
                    hUsage <- peek usagePtr
                    putStrLn ("Limit: " ++ (show hLimit) ++ " Usage: " ++ (show hUsage))
                    putStrLn "Done")))

testAlloclimitFail :: IO ()
testAlloclimitFail = do
    putStrLn "Testing Alloclimit Failure"
    engineFPtr <- newRosieEngine
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 8199) (\limitPtr ->
            with (fromInteger 8191) (\limit2Ptr ->
                with (fromInteger 0) (\usagePtr -> do
                    errorCode <- cRosieAllocLimit enginePtr limitPtr usagePtr
                    if errorCode /= 0
                    then throwM $ TestException "testAlloclimitFail" "Failed to get alloclimit"
                    else do
                        hLimit <- peek limitPtr
                        hUsage <- peek usagePtr
                        putStrLn ("Limit: " ++ (show hLimit) ++ " Usage: " ++ (show hUsage))
                        errorCode2 <- cRosieAllocLimit enginePtr limit2Ptr usagePtr
                        if errorCode2 /= 0
                        then do
                            putStrLn "Failed to get alloclimit 2 as expected"
                            putStrLn "Done"
                        else do
                            hLimit2 <- peek limit2Ptr
                            hUsage2 <- peek usagePtr
                            throwM $ TestException "testAllocLimitFail" ("Unexpectedly got alloclimit. Limit 2: " ++ (show hLimit2) ++ " Usage 2: " ++ (show hUsage2))))))

testMonadicAlloclimit :: IO ()
testMonadicAlloclimit = do
    putStrLn "Testing Monadic Alloclimit"
    engineFPtr <- newRosieEngine
    (engine2FPtr, limit, usage) <- allocLimit engineFPtr 8199
    putStrLn ("Limit: " ++ (show limit) ++ " Usage: " ++ (show usage))
    putStrLn "Done"

testImport :: IO ()
testImport = do
    putStrLn "Testing Import"
    engineFPtr <- newRosieEngine
    packageNameFPtr <- newRosieString "net"
    asFPtr <- emptyRosieString
    actualPackageNameFPtr <- emptyRosieString
    messagesFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr packageNameFPtr (\packageNamePtr ->
                withForeignPtr asFPtr (\asPtr ->
                    withForeignPtr actualPackageNameFPtr (\actualPackageNamePtr ->
                        withForeignPtr messagesFPtr (\messagesPtr -> do
                            errorCode <- cRosieImport enginePtr okPtr packageNamePtr asPtr actualPackageNamePtr messagesPtr
                            hOk <- peek okPtr
                            if errorCode /= 0 || hOk == 0
                            then do
                                hErr <- peek messagesPtr
                                if (rosieStringLength hErr) /= 0
                                then do
                                    hErrors <- peekCStringLen (rosieStringChars hErr)
                                    throwM $ TestException "testImport" ("Import failed: " ++ hErrors ++ " Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                                else throwM $ TestException "testImport" ("Import failed: No error message. Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                            else do
                                hPn <- peek actualPackageNamePtr
                                if (rosieStringLength hPn) /= 0
                                then do
                                    packageName <- peekCStringLen (rosieStringChars hPn)
                                    putStrLn ("Imported " ++ packageName)
                                    putStrLn "Done"
                                else throwM $ TestException "testImport" "Import failed: No package name"))))))

testMonadicImport :: IO ()
testMonadicImport = do
    putStrLn "Testing Monadic Import"
    engineFPtr <- newRosieEngine
    packageNameFPtr <- newRosieString "net"
    (engine2FPtr, actualPackageNameFPtr) <- importPackage engineFPtr packageNameFPtr
    withForeignPtr actualPackageNameFPtr (\actualPackageNamePtr -> do
        hPn <- peek actualPackageNamePtr
        if (rosieStringLength hPn) /= 0
        then do
            packageName <- peekCStringLen (rosieStringChars hPn)
            putStrLn ("Imported " ++ packageName)
            putStrLn "Done"
        else throwM $ TestException "testMonadicImport" "Import failed: No package name")

testLoadfile :: IO ()
testLoadfile = do
    putStrLn "Testing Loadfile"
    engineFPtr <- newRosieEngine
    fileNameFPtr <- newRosieString "test.rpl"
    packageNameFPtr <- emptyRosieString
    messagesFPtr <- emptyRosieString
    withForeignPtr engineFPtr (\enginePtr ->
        with (fromInteger 0) (\okPtr ->
            withForeignPtr fileNameFPtr (\fileNamePtr ->
                withForeignPtr packageNameFPtr (\packageNamePtr ->
                    withForeignPtr messagesFPtr (\messagesPtr -> do
                        errorCode <- cRosieLoadfile enginePtr okPtr fileNamePtr packageNamePtr messagesPtr
                        hOk <- peek okPtr
                        if errorCode /= 0 || hOk == 0
                        then do
                            hErr <- peek messagesPtr
                            if (rosieStringLength hErr) /= 0
                            then do
                                hErrors <- peekCStringLen (rosieStringChars hErr)
                                throwM $ TestException "testLoadFile" ("Loadfile failed: " ++ hErrors ++ " Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                            else throwM $ TestException "testLoadFile" ("Loadfile failed: No error message. Error code: " ++ (show errorCode) ++ " Lua error code: " ++ (show hOk))
                        else do
                            hPn <- peek packageNamePtr
                            if (rosieStringLength hPn) /= 0
                            then do
                                packageName <- peekCStringLen (rosieStringChars hPn)
                                putStrLn ("Loaded file with package: " ++ packageName)
                                putStrLn "Done"
                            else throwM $ TestException "testLoadFile" "Loadfile failed: No package name")))))

testMonadicLoadfile :: IO ()
testMonadicLoadfile = do
    putStrLn "Testing Monadic Loadfile"
    fileNameFPtr <- newRosieString "test.rpl"
    (engine2FPtr, packageNameFPtr) <- loadEngineFromFile fileNameFPtr
    withForeignPtr packageNameFPtr (\packageNamePtr -> do
        hPn <- peek packageNamePtr
        if (rosieStringLength hPn) /= 0
        then do
            packageName <- peekCStringLen (rosieStringChars hPn)
            putStrLn ("Loaded package from file: " ++ packageName)
            putStrLn "Done"
        else throwM $ TestException "testMonadicLoadFile" "Loadfile: No package name")

testMatch :: IO ()
testMatch = do
    putStrLn "Testing Match"
    let start = 2 :: Int32
    engineFPtr <- newRosieEngine
    expression1FPtr <- newRosieString "[:digit:]+"
    (engine2FPtr, pat) <- compileExpression engineFPtr expression1FPtr
    inputFPtr <- newRosieString "321"
    matchFPtr <- emptyRosieMatch
    withForeignPtr engine2FPtr (\enginePtr ->
        withCString "json" (\encoder ->
            withForeignPtr inputFPtr (\inputPtr ->
                withForeignPtr matchFPtr (\matchPtr -> do
                    errorCode <- cRosieMatch enginePtr pat start encoder inputPtr matchPtr
                    if errorCode /= 0
                    then do
                        throwM $ TestException "testMatch" ("Match failed with error code: " ++ (show errorCode))
                    else do
                        hMatch <- peek matchPtr
                        putStrLn (show hMatch)
                        hMatchString <- peekCStringLen (rosieMatchChars hMatch)
                        putStrLn ("Match: " ++ (show $ parseMatch hMatchString))
                        putStrLn "Done"))))

testMonadicMatch :: IO ()
testMonadicMatch = do
    putStrLn "Testing Monadic Match"
    let start = 2 :: Int32
    engineFPtr <- newRosieEngine
    expression1FPtr <- newRosieString "[:digit:]+"
    (engine2FPtr, pat) <- compileExpression engineFPtr expression1FPtr
    inputFPtr <- newRosieString "321"
    matchResult <- runMaybeT $ matchPattern engine2FPtr start pat inputFPtr
    case matchResult of
        Nothing -> putStrLn "Failed to match pattern"
        Just matchFPtr ->
            withForeignPtr matchFPtr (\matchPtr -> do
                hMatch <- peek matchPtr
                putStrLn (show hMatch)
                hMatchString <- peekCStringLen (rosieMatchChars hMatch)
                putStrLn ("Match: " ++ (show $ parseMatch hMatchString))
                putStrLn "Done")

-- WARNING: This test will throw an unrecoverable exception
testMonadicLoadFail :: IO ()
testMonadicLoadFail = do
    putStrLn "Testing Monadic Loading (Failure expected)"
    sourceFPtr <- newRosieString "package x; foo = \"foo\""
    (engineFPtr, packageNameFPtr) <- loadEngine sourceFPtr
    expression1FPtr <- newRosieString "[:foobar:]+"
    engine2FPtr <- compileExpression engineFPtr expression1FPtr
    putStrLn "Done"

-- WARNING: This test will throw an unrecoverable exception
testMonadicAlloclimitFail :: IO ()
testMonadicAlloclimitFail = do
    putStrLn "Testing Monadic Alloclimit (Failure expected)"
    engineFPtr <- newRosieEngine
    (engine2FPtr, limit, usage) <- allocLimit engineFPtr 8191
    putStrLn ("Limit: " ++ (show limit) ++ " Usage: " ++ (show usage))
    putStrLn "Done"

testRosie :: IO ()
testRosie = do
    putStrLn "Rosie Test"
    testUnsafeInit
    testSaferInit
    testSaferLoad
    testMonadicLoad
    testConfig
    testMonadicConfig
    testLibpath
    testAlloclimitUsage
    testAlloclimit
    testAlloclimitFail
    testMonadicAlloclimit
    testImport
    testMonadicImport
    testLoadfile
    testMonadicLoadfile
    testMatch
    testMonadicMatch
    --putStrLn "This test will throw an unrecoverable exception"
    --onException testMonadicLoadFail (putStrLn "Test failed as expected")
    --onException testMonadicAlloclimitFail (putStrLn "Test failed as expected")
